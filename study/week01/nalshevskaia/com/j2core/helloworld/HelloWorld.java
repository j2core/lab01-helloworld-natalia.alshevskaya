package study.week01.nalshevskaia.com.j2core.helloworld;

public class HelloWorld{

    private static final String PHRASE ="Hello World!";

    public static void main(String[] args){
        System.out.println(PHRASE);
    }
}
